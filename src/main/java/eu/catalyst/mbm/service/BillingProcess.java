package eu.catalyst.mbm.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import eu.catalyst.mbm.payload.Invoice;
import eu.catalyst.mbm.payload.MarketActionCounterOffer;
import eu.catalyst.mbm.payload.PayloadMEMO2MBMNotFlex;
import eu.catalyst.mbm.payload.Transaction;
import eu.catalyst.mbm.payload.MarketSessionPlain;

@Stateless
@Path("/billingProcess/")
public class BillingProcess {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(BillingProcess.class);

	public BillingProcess() {
	}

	@POST
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public void postBillingProcess(PayloadMEMO2MBMNotFlex inputPayloadMEMO2MBMNotFlex) {

		log.debug("-> postBillingProcess");

		List<MarketActionCounterOffer> myMarketActionCounterOfferList = inputPayloadMEMO2MBMNotFlex
				.getMarketActionCounterOfferList();

		try {

			for (Iterator<MarketActionCounterOffer> iterator = myMarketActionCounterOfferList.iterator(); iterator
					.hasNext();) {
				MarketActionCounterOffer myMarketActionCounterOffer = iterator.next();

				ArrayList<Transaction> transactionList = new ArrayList<Transaction>();

				Transaction myTransaction = new Transaction();
				myTransaction.setDateTime(new Date());
				myTransaction.setMarketActionCounterOfferid(myMarketActionCounterOffer.getId());
				transactionList.add(myTransaction);

				URL url4 = new URL(inputPayloadMEMO2MBMNotFlex.getInformationBrokerServerUrl() + "/marketplace/"
						+ inputPayloadMEMO2MBMNotFlex.getMarketSession().getMarketplaceid().getId().intValue()
						+ "/marketsessions/" + inputPayloadMEMO2MBMNotFlex.getMarketSession().getId().intValue()
						+ "/transactions/");

				log.debug("Start - Rest url4: " + url4.toString());
				HttpURLConnection connService4 = (HttpURLConnection) url4.openConnection();
				connService4.setDoOutput(true);
				connService4.setRequestMethod("POST");
				connService4.setRequestProperty("Content-Type", "application/json");
				connService4.setRequestProperty("Accept", "application/json");
				connService4.setRequestProperty("Authorization", inputPayloadMEMO2MBMNotFlex.getTokenMBM());

				GsonBuilder gb4 = new GsonBuilder();
				gb4.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson4 = gb4.create();
				String record4 = gson4.toJson(transactionList);
				log.debug("Rest Request: " + record4);

				OutputStream os4 = connService4.getOutputStream();
				os4.write(record4.getBytes());

				int myResponseCode4 = connService4.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode4);

				BufferedReader br4 = new BufferedReader(new InputStreamReader((connService4.getInputStream())));
				Type listType4 = new TypeToken<ArrayList<Transaction>>() {
				}.getType();

				ArrayList<Transaction> returnTransactionList = new ArrayList<Transaction>();
				returnTransactionList = gson4.fromJson(br4, listType4);
				log.debug("payload received: " + gson4.toJson(returnTransactionList));
				
				os4.flush();
				os4.close();
				br4.close();
				connService4.disconnect();

				// Create list(1 item) with Invoice with current Transaction and
				// current date
				// set fixedFee field : property value FIXEDFEE always
				// set amount field : property value AMOUNT if ActionStatus is
				// "delivered"
				// set penalty field : property value PENALTY if ActionStatus is
				// "not_delivered"
				// invoke POST
				// "/marketplace/{marketId}/marketsessions/{sessionId}/invoices"
				// and retrieve list(1 item) of updated Invoice

				ArrayList<Invoice> invoiceList = new ArrayList<Invoice>();
				Invoice myInvoice = new Invoice();
				myInvoice.setDate(new Date());
				myInvoice.setTransactionid(returnTransactionList.get(0).getId());

				BigDecimal myamount = myMarketActionCounterOffer.getExchangedValue()
						.multiply(inputPayloadMEMO2MBMNotFlex.getClearingPrice());
				myInvoice.setPenalty(new BigDecimal(0));
				myInvoice.setFixedFee(new BigDecimal(0));

				myInvoice.setAmount(myamount);

				invoiceList.add(myInvoice);

				URL url5 = new URL(inputPayloadMEMO2MBMNotFlex.getInformationBrokerServerUrl() + "/marketplace/"
						+ inputPayloadMEMO2MBMNotFlex.getMarketSession().getMarketplaceid().getId().intValue()
						+ "/marketsessions/" + inputPayloadMEMO2MBMNotFlex.getMarketSession().getId().intValue()
						+ "/invoices/");
				// only for test + myBid.getMarketSessionid().getId().intValue()
				// + "/actions/invoices");

				log.debug("Start - Rest url5: " + url5.toString());
				HttpURLConnection connService5 = (HttpURLConnection) url5.openConnection();
				connService5.setDoOutput(true);
				connService5.setRequestMethod("POST");
				connService5.setRequestProperty("Content-Type", "application/json");
				connService5.setRequestProperty("Accept", "application/json");
				connService5.setRequestProperty("Authorization", inputPayloadMEMO2MBMNotFlex.getTokenMBM());

				GsonBuilder gb5 = new GsonBuilder();
				gb5.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson5 = gb5.create();
				String record5 = gson5.toJson(invoiceList);
				log.debug("Rest Request: " + record5);

				OutputStream os5 = connService5.getOutputStream();
				os5.write(record5.getBytes());

				int myResponseCode5 = connService5.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode5);

				BufferedReader br5 = new BufferedReader(new InputStreamReader((connService5.getInputStream())));
				Type listType5 = new TypeToken<ArrayList<Invoice>>() {
				}.getType();

				ArrayList<Invoice> returnInvoiceList = new ArrayList<Invoice>();
				returnInvoiceList = gson5.fromJson(br5, listType5);
				log.debug("payload received: " + gson5.toJson(returnInvoiceList));

				os5.flush();
				os5.close();
				br5.close();
				connService5.disconnect();

			}
			
			// update Status of Session to completed (5)
			inputPayloadMEMO2MBMNotFlex.getMarketSession().getSessionStatusid().setId(5); // completed
			invokeUpdateMarketSession(inputPayloadMEMO2MBMNotFlex);

		} catch (Exception e) {
			e.printStackTrace();
		}

		log.debug("<- postBillingProcess");

	}

	public static void invokeUpdateMarketSession(PayloadMEMO2MBMNotFlex inputPayloadMEMO2MBMNotFlex) {

		log.debug("-> invokeUpdateMarketSession");

		try {
			URL url = new URL(inputPayloadMEMO2MBMNotFlex.getInformationBrokerServerUrl() + "/marketplace/"
					+ inputPayloadMEMO2MBMNotFlex.getMarketSession().getMarketplaceid().getId() + "/marketsessions/"
					+ inputPayloadMEMO2MBMNotFlex.getMarketSession().getId().intValue() + "/");

			log.debug("Rest url: " + url.toString());

			HttpURLConnection connService = (HttpURLConnection) url.openConnection();
			connService.setDoOutput(true);
			connService.setRequestMethod("PUT");
			connService.setRequestProperty("Accept", "application/json");
			connService.setRequestProperty("Content-Type", "application/json");
			connService.setRequestProperty("Authorization", inputPayloadMEMO2MBMNotFlex.getTokenMBM());
			GsonBuilder gb = new GsonBuilder();

			// gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			Gson gson = gb.create();

			// setting MarketSession only Ids
			MarketSessionPlain myMarketSessionPlain = new MarketSessionPlain();
			myMarketSessionPlain.setId(inputPayloadMEMO2MBMNotFlex.getMarketSession().getId());
			myMarketSessionPlain
					.setSessionStartTime(inputPayloadMEMO2MBMNotFlex.getMarketSession().getSessionStartTime());
			myMarketSessionPlain.setSessionEndTime(inputPayloadMEMO2MBMNotFlex.getMarketSession().getSessionEndTime());
			myMarketSessionPlain
					.setDeliveryStartTime(inputPayloadMEMO2MBMNotFlex.getMarketSession().getDeliveryStartTime());
			myMarketSessionPlain
					.setDeliveryEndTime(inputPayloadMEMO2MBMNotFlex.getMarketSession().getDeliveryEndTime());
			myMarketSessionPlain
					.setSessionStatusid(inputPayloadMEMO2MBMNotFlex.getMarketSession().getSessionStatusid().getId());
			myMarketSessionPlain
					.setMarketplaceid(inputPayloadMEMO2MBMNotFlex.getMarketSession().getMarketplaceid().getId());
			myMarketSessionPlain.setFormid(inputPayloadMEMO2MBMNotFlex.getMarketSession().getFormid().getId());
			myMarketSessionPlain.setClearingPrice(inputPayloadMEMO2MBMNotFlex.getMarketSession().getClearingPrice());

			// String record = gson.toJson(inputPayloadMEMO2MCMNotFlex.getMarketSession());
			String record = gson.toJson(myMarketSessionPlain);

			log.debug("Rest Request: " + record);

			OutputStream os = connService.getOutputStream();
			os.write(record.getBytes());

			int myResponseCode = connService.getResponseCode();
			log.debug("HTTP error code :" + myResponseCode);
			os.flush();
			os.close();
			connService.disconnect();

		} catch (

		Exception e) {
			e.printStackTrace();
		}

		log.debug("<- invokeUpdateMarketSession");
		return;
	}

}
