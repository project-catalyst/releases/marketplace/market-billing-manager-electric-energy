/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.catalyst.mbm.payload;

import java.io.Serializable;
import java.util.Collection;

public class SessionStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;

	private String status;

	public SessionStatus() {
	}

	public SessionStatus(Integer id) {
		this.id = id;
	}

	public SessionStatus(Integer id, String status) {
		this.id = id;
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {

		if (!(object instanceof SessionStatus)) {
			return false;
		}
		SessionStatus other = (SessionStatus) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "eu.catalyst.marketplace.model.SessionStatus[ id=" + id + " ]";
	}

}
