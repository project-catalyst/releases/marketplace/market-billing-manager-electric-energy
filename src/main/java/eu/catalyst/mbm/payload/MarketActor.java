/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.catalyst.mbm.payload;

import java.io.Serializable;
import java.util.Collection;

public class MarketActor implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String companyName;
	private String email;
	private String phone;
	private String representativeName;
	private String vat;
	private MarketActorType marketActorTypeid;

	public MarketActor() {
	}

	public MarketActor(Integer id) {
		this.id = id;
	}

	public MarketActor(Integer id, String companyName, String email, String phone, String representativeName,
			String vat) {
		this.id = id;
		this.companyName = companyName;
		this.email = email;
		this.phone = phone;
		this.representativeName = representativeName;
		this.vat = vat;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRepresentativeName() {
		return representativeName;
	}

	public void setRepresentativeName(String representativeName) {
		this.representativeName = representativeName;
	}

	public String getVat() {
		return vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	public MarketActorType getMarketActorTypeid() {
		return marketActorTypeid;
	}

	public void setMarketActorTypeid(MarketActorType marketActorTypeid) {
		this.marketActorTypeid = marketActorTypeid;
	}

	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {

		if (!(object instanceof MarketActor)) {
			return false;
		}
		MarketActor other = (MarketActor) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "eu.catalyst.marketplace.model.MarketActor[ id=" + id + " ]";
	}

}
