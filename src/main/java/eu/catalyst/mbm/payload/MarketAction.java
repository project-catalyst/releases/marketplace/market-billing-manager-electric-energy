
package eu.catalyst.mbm.payload;

import java.io.Serializable;

import java.math.BigDecimal;
import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import eu.catalyst.mbm.global.DateDeSerializer;
import eu.catalyst.mbm.global.DateSerializer;

public class MarketAction implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
    
	@JsonDeserialize(using = DateDeSerializer.class)
    @JsonSerialize(using = DateSerializer.class)
	private Date date;

    @JsonDeserialize(using = DateDeSerializer.class)
    @JsonSerialize(using = DateSerializer.class)
	private Date actionStartTime;
    
    @JsonDeserialize(using = DateDeSerializer.class)
    @JsonSerialize(using = DateSerializer.class)
	private Date actionEndTime;

	private BigDecimal value;

	private String uom;

	private BigDecimal price;

	private String deliveryPoint;

	private Integer statusid;

	private Integer marketSessionid;

	private Integer marketActorid;

	private Integer formid;

	private Integer actionTypeid;

	private Integer loadid;

	public MarketAction() {
	}

	public MarketAction(Integer id) {
		this.id = id;
	}

	public MarketAction(Integer id, Date date, Date actionStartTime, Date actionEndTime, BigDecimal value, String uom,
			BigDecimal price) {
		this.id = id;
		this.date = date;
		this.actionStartTime = actionStartTime;
		this.actionEndTime = actionEndTime;
		this.value = value;
		this.uom = uom;
		this.price = price;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getActionStartTime() {
		return actionStartTime;
	}

	public void setActionStartTime(Date actionStartTime) {
		this.actionStartTime = actionStartTime;
	}

	public Date getActionEndTime() {
		return actionEndTime;
	}

	public void setActionEndTime(Date actionEndTime) {
		this.actionEndTime = actionEndTime;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getDeliveryPoint() {
		return deliveryPoint;
	}

	public void setDeliveryPoint(String deliveryPoint) {
		this.deliveryPoint = deliveryPoint;
	}

	public Integer getStatusid() {
		return statusid;
	}

	public void setStatusid(Integer statusid) {
		this.statusid = statusid;
	}

	public Integer getMarketSessionid() {
		return marketSessionid;
	}

	public void setMarketSessionid(Integer marketSessionid) {
		this.marketSessionid = marketSessionid;
	}

	public Integer getMarketActorid() {
		return marketActorid;
	}

	public void setMarketActorid(Integer marketActorid) {
		this.marketActorid = marketActorid;
	}

	public Integer getFormid() {
		return formid;
	}

	public void setFormid(Integer formid) {
		this.formid = formid;
	}

	public Integer getLoadid() {
		return loadid;
	}

	public void setLoadid(Integer loadid) {
		this.loadid = loadid;
	}

	public Integer getActionTypeid() {
		return actionTypeid;
	}

	public void setActionTypeid(Integer actionTypeid) {
		this.actionTypeid = actionTypeid;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {

		if (!(object instanceof MarketAction)) {
			return false;
		}
		MarketAction other = (MarketAction) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "eu.catalyst.marketclearing.model.MarketAction[ id=" + id + " ]";
	}

}
