/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.catalyst.mbm.payload;

import java.io.Serializable;

public class Marketplace implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private MarketServiceType marketServiceTypeid;
	private MarketActor dsoid;
	private MarketActor marketOperatorid;

	public Marketplace() {
	}

	public Marketplace(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public MarketServiceType getMarketServiceTypeid() {
		return marketServiceTypeid;
	}

	public void setMarketServiceTypeid(MarketServiceType marketServiceTypeid) {
		this.marketServiceTypeid = marketServiceTypeid;
	}
	
	public MarketActor getDsoid() {
		return dsoid;
	}

	public void setDsoid(MarketActor dsoid) {
		this.dsoid = dsoid;
	}

	public MarketActor getMarketOperatorid() {
		return marketOperatorid;
	}

	public void setMarketOperatorid(MarketActor marketOperatorid) {
		this.marketOperatorid = marketOperatorid;
	}

	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {

		if (!(object instanceof Marketplace)) {
			return false;
		}
		Marketplace other = (Marketplace) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "eu.catalyst.marketplace.model.Marketplace[ id=" + id + " ]";
	}

}