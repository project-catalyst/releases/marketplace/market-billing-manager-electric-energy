package eu.catalyst.mbm.payload;

import java.math.BigDecimal;
import java.util.List;
import java.io.Serializable;
public class PayloadMEMO2MBMNotFlex implements Serializable {
    private static final long serialVersionUID = 1L;


	public PayloadMEMO2MBMNotFlex() {
	}

	private BigDecimal clearingPrice;
	private MarketSession marketSession;
	private List<PrioritisedAction> prioritisedActionsList;
	private List<MarketActionCounterOffer> marketActionCounterOfferList;

	private Integer informationBrokerId;
	private String informationBrokerServerUrl;
	private String tokenMEMO;
	private String marketBillingManagerUrl;
	private String tokenMBM;

	public MarketSession getMarketSession() {
		return marketSession;
	}

	public void setMarketSession(MarketSession marketSession) {
		this.marketSession = marketSession;
	}

	public List<PrioritisedAction> getPrioritisedActionsList() {
		return prioritisedActionsList;
	}

	public void setPrioritisedActionsList(List<PrioritisedAction> prioritisedActionsList) {
		this.prioritisedActionsList = prioritisedActionsList;
	}

	public List<MarketActionCounterOffer> getMarketActionCounterOfferList() {
		return marketActionCounterOfferList;
	}

	public void setMarketActionCounterOfferList(List<MarketActionCounterOffer> marketActionCounterOfferList) {
		this.marketActionCounterOfferList = marketActionCounterOfferList;
	}

	public String getInformationBrokerServerUrl() {
		return informationBrokerServerUrl;
	}

	public void setInformationBrokerServerUrl(String informationBrokerServerUrl) {
		this.informationBrokerServerUrl = informationBrokerServerUrl;
	}

	public String getTokenMEMO() {
		return tokenMEMO;
	}

	public void setTokenMEMO(String tokenMEMO) {
		this.tokenMEMO = tokenMEMO;
	}

	public String getTokenMBM() {
		return tokenMBM;
	}

	public void setTokenMBM(String tokenMBM) {
		this.tokenMBM = tokenMBM;
	}

	public String getMarketBillingManagerUrl() {
		return marketBillingManagerUrl;
	}

	public void setMarketBillingManagerUrl(String marketBillingManagerUrl) {
		this.marketBillingManagerUrl = marketBillingManagerUrl;
	}

	public BigDecimal getClearingPrice() {
		return clearingPrice;
	}

	public void setClearingPrice(BigDecimal clearingPrice) {
		this.clearingPrice = clearingPrice;
	}

	public Integer getInformationBrokerId() {
		return informationBrokerId;
	}

	public void setInformationBrokerId(Integer informationBrokerId) {
		this.informationBrokerId = informationBrokerId;
	}

}