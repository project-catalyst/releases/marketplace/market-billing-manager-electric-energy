/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.catalyst.mbm.payload;

import java.io.Serializable;


public class MarketplacehasMarketActorPK implements Serializable {
	 private static final long serialVersionUID = 1L;
    private int id;

    private int marketplaceid;

    private int marketActorid;

    public MarketplacehasMarketActorPK() {
    }

    public MarketplacehasMarketActorPK(int id, int marketplaceid, int marketActorid) {
        this.id = id;
        this.marketplaceid = marketplaceid;
        this.marketActorid = marketActorid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMarketplaceid() {
        return marketplaceid;
    }

    public void setMarketplaceid(int marketplaceid) {
        this.marketplaceid = marketplaceid;
    }

    public int getMarketActorid() {
        return marketActorid;
    }

    public void setMarketActorid(int marketActorid) {
        this.marketActorid = marketActorid;
    }


    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) marketplaceid;
        hash += (int) marketActorid;
        return hash;
    }


    public boolean equals(Object object) {
        
        if (!(object instanceof MarketplacehasMarketActorPK)) {
            return false;
        }
        MarketplacehasMarketActorPK other = (MarketplacehasMarketActorPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.marketplaceid != other.marketplaceid) {
            return false;
        }
        if (this.marketActorid != other.marketActorid) {
            return false;
        }
        return true;
    }

 
    public String toString() {
        return "eu.catalyst.marketplace.model.MarketplacehasMarketActorPK[ id=" + id + ", marketplaceid=" + marketplaceid + ", marketActorid=" + marketActorid + " ]";
    }

}
