package eu.catalyst.mbm.payload;

import java.io.Serializable;
import java.util.Date;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import eu.catalyst.mbm.global.DateDeSerializer;
import eu.catalyst.mbm.global.DateSerializer;

public class Transaction implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	
	@JsonDeserialize(using = DateDeSerializer.class)
    @JsonSerialize(using = DateSerializer.class)
	private Date dateTime;

	private Integer marketActionCounterOfferid;

	public Transaction() {
	}

	public Transaction(Integer id) {
		this.id = id;
	}

	public Transaction(Integer id, Date dateTime) {
		this.id = id;
		this.dateTime = dateTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public Integer getMarketActionCounterOfferid() {
		return marketActionCounterOfferid;
	}

	public void setMarketActionCounterOfferid(Integer marketActionCounterOfferid) {
		this.marketActionCounterOfferid = marketActionCounterOfferid;
	}

}
