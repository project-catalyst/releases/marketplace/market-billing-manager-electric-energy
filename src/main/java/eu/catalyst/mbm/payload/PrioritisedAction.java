package eu.catalyst.mbm.payload;
import java.io.Serializable;
public class PrioritisedAction implements Serializable {
    private static final long serialVersionUID = 1L;

	private MarketAction marketaction;

	private Integer priority;

	public PrioritisedAction() {
	}

	public MarketAction getMarketaction() {
		return marketaction;
	}

	public void setMarketaction(MarketAction marketaction) {
		this.marketaction = marketaction;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

}
